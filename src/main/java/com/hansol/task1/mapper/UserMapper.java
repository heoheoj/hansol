package com.hansol.task1.mapper;

import com.hansol.task1.model.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMapper {
  //  @Select("SELECT * FROM user")
    List<User> findAll();

    User findById(Long id);

    User findByCode(String code);

    User findByPhone(String phone);

    User findByTask(String task);

    User findByCompany(String company);

    User findByRank(String rank);

    User findByKind(String kind);

    void update(Long id);

    void delete(Long id);
}
