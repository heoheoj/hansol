package com.hansol.task1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tesk1Application {

    public static void main(String[] args) {
        SpringApplication.run(Tesk1Application.class, args);
    }

}
