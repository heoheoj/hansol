package com.hansol.task1.service;

import com.hansol.task1.model.User;

import java.util.List;

public interface UserService {
    public List<User> readUser() throws Exception;
    public void modify(User user) throws Exception;
    public void remove(Long id) throws Exception;
   // public void search() throws Exception;



}
